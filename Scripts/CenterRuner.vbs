'Create QTP object
Set QTP = CreateObject("QuickTest.Application")
QTP.Launch
QTP.Visible = TRUE
 
'Open QTP Test
Test_Result_Path ="D:\10X_Automate\Scripts\SCB_10X_Automate"
'Test_Result_Path =WScript.Arguments.Item(3)
QTP.Open Test_Result_Path, TRUE 'Set the QTP test path
  
'Set Result location
Set qtpResultsOpt = CreateObject("QuickTest.RunResultsOptions")
'qtpResultsOpt.ResultsLocation = "Result path" 'Set the results location


 
'Set the Test Parameters
Set qtpParams = QTP.Test.ParameterDefinitions.GetParameters()

'Set the value for test environment through command line
On Error Resume Next

ParamName1 = "Param1"
qtpParams.Item(ParamName1).Value = WScript.Arguments.Item(0)

ParamName2 = "Param2"
qtpParams.Item(ParamName2).Value = WScript.Arguments.Item(1)

On Error GoTo 0
 
  
'Run QTP test
QTP.Test.Run qtpResultsOpt, FALSE, qtpParams

'Close QTP
'QTP.Test.Close
'QTP.Quit

